<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Auth::routes();
//read admin panel
Route::get('/home', 'AdminController@admin')->middleware('is_admin')->name('admin');
//read ordertable
Route::get('/orders' , 'orderitemController@index')->name('view.order');
Route::get('/orders/{id}','orderitemController@store')->name('store.order');
Route::patch('/orders/{id}','orderitemController@updateStore')->name('updateStore.order');


//display admin order table
Route::get('/home/orders', 'AdminController@order')->name('admin.order');

//display user panel
Route::get('/index', 'DisplayController@index')->name('index');
Route::get('/', 'DisplayController@undercons')->name('undercons');

//create
Route::get('/create' , 'BookController@create')->name('add.book');
Route::post('/create', 'BookController@store' )->name('store.book');


//edit tampilkan view edit
Route::get('/home/{id}/edit' , 'BookController@edit')->name('edit.book');
Route::patch('home/{id}/edit','BookController@update')->name('update.book');


//delete
Route::delete('/home/{id}/delete' , 'BookController@destroy')->name('delete.book');
