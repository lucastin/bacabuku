@extends('layouts.admin')
@section('content')

    <div id="wrapper">

        <!-- Sidebar -->
        <ul class="sidebar navbar-nav">
            <li class="nav-item active">
                <a class="nav-link" href="#">
                    <i class="fas fa-fw fa-tachometer-alt"></i>
                    <span>Dashboard</span>
                </a>
            </li>

        </ul>

        <div id="content-wrapper">

            <div class="container-fluid">

                <!-- Breadcrumbs-->
                <ol class="breadcrumb">
                    <li class="breadcrumb-item">
                        <p  class="lead">AddBook</p>
                    </li>

                </ol>

                <!-- Icon Cards-->
                <div class="row">
                    <div class="container">
                        <form method="post" action="{{route('store.book')}}" enctype="multipart/form-data" >
                            {{csrf_field()}}
                            <div class="form-group">
                                <label for="title">Judul Buku</label>
                                <input type="text" name="title" class="form-control" placeholder="Masukan Judul Buku">
                            </div>
                            <div class="form-group">
                                <label for="pengarang">Pengarang Buku</label>
                                <input type="text" name="pengarang" class="form-control" placeholder="Masukan Judul Buku">
                            </div>
                            <div class="form-group">
                                <label for="penerbit">Penerbit Buku</label>
                                <input type="text" name="penerbit" class="form-control" placeholder="Masukan Judul Buku">
                            </div>
                            <div class="form-group">
                                <label for="synopsis">Synopsis Buku</label>
                                <textarea class="form-control" style="min-width: 25%;" name="synopsis"></textarea>
                            </div>
                            <div class="form-group">
                                <label for="price">Harga Buku</label>
                                <input type="number" class="form-control" min="0"  name="price">
                            </div>
                            <div class="form-group">
                                <label for="image">Cover Buku</label>
                                <input  name="image" type="file" class="form-control-file" >

                            </div>
                            <div class="form-group">

                                <input type="submit" value="Add Book"  class="btn btn-success">
                            </div>
                        </form>
                    </div>



                </div>



                <!-- DataTables Example -->


            </div>
        </div>
@endsection