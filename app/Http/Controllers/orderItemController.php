<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Buku;
use App\orderitem;
use Auth;

class orderItemController extends Controller
{
    //

    function index(){

        $buku = Buku::paginate(4);
        return view('Order.orderIndex',compact('buku' ));
    }

    function store($id){
        $buku = Buku::find($id);
       $order = orderitem::all();


        //check user login
        if (Auth::check())
        {
            $user = Auth::user();
        }
        return view('Order.orderform',compact('buku','user' , 'order'));



    }
    function updateStore($id){
//        $buku = Buku::find($id);
        orderitem::create(
            [
                'user_id' => \request('idUser'),
                'buku_id' => \request('idBuku'),
                'alamat' => \request('alamat'),
                'price' => \request('harga'),
                'user_name' => \request('namaPenerima'),
                'user_phone' => \request('phoneNumber'),
                'user_email' => \request('emailPenerima')
            ]


        );
        return redirect()->route('view.order')->with('success' , 'order sent');


    }
}
