<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Buku;
use App\orderitem;

class AdminController extends Controller
{
    //
    public function __construct()
    {
        $this->middleware('auth');
    }

    //read

    public function admin()
    {
        $buku = Buku::paginate(5);
        $oderitems = orderitem::all();
        return view('admin' , compact('buku' , 'oderitems'));
    }

    function order(){
        $buku = Buku::all();
        $oderitems = orderitem::paginate(5);
        return view('adminorder' , compact('buku','oderitems'));
    }

}
