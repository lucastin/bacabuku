<?php

namespace App\Http\Controllers;

//use Illuminate\Http\Request;
use Request;
use App\Buku;

class BookController extends Controller
{
    //adding a book form
    public function create(){

        return view('Book.add');

    }


   //store it to database
    public function store(){

        //file upload
        $status = Request::hasFile('image');
        $input['imagename'] = "";
        if($status){
            $image =  \request('image');
            $input['imagename'] = time().".".$image->extension();
            $dest= public_path('storage/Images');
            $image->move($dest,$input['imagename']);

        }

      Buku::create(
        [
            'title' => \request('title'),
            'pengarang' => \request('pengarang'),
            'penerbit' => \request('penerbit'),
            'synopsis' => \request('synopsis'),
            'price' => \request('price'),
            'image' => $input['imagename']

        ]
      );
      return redirect()->route('admin');

    }

    //edit

    public function edit($id){
        $buku = Buku::find($id);
        return view('Book.edit' , compact(['buku']));

    }

    //bosen tjoek :(

    public function update($id){
        $buku = Buku::find($id);
        $status = Request::hasFile('image');
        $input['imagename'] = $buku->image;
        if($status){
            $image =  \request('image');
            $input['imagename'] = time().".".$image->extension();
            $dest= public_path('/Images');
            $image->move($dest,$input['imagename']);

        }
        $buku->update(
            [
                'title' => \request('title'),
                'pengarang' => \request('pengarang'),
                'penerbit' => \request('penerbit'),
                'synopsis' => \request('synopsis'),
                'price' => \request('price'),
                'image' => $input['imagename']


            ]
        );
        return redirect()->route('admin');

    }

    public function destroy($id){
        $buku = Buku::find($id);
        $buku->delete();
        return redirect()->route('admin');
    }
}
