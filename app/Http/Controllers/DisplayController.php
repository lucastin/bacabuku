<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Buku;

class DisplayController extends Controller
{
    //

    public function index(){
        return view('index');
    }

    public function undercons(){
       $buku =  Buku::all();
        return view('undercons' , compact('buku'));
    }
}
