<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class orderitem extends Model
{
    //
    protected  $fillable = ['user_id','buku_id','alamat','price' , 'user_name' , 'user_phone' , 'user_email'];
}
